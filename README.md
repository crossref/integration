# Integration

This projects requires an up-to-date version of **Docker** installed locally. 

## Quick execution: Docker + Colima

This method will allow you to run whole integration project *within* a linux+docker instance. 

* install brew if not installed
* install [colima](https://github.com/abiosoft/colima) if not installed
* launch colima with `colima start`
* clone this project
* add this project base directory to the `PATH` env VAR

-----------------------------------------------------------------------------------------------
| service       | ports                                               |
|---------------|-----------------------------------------------------|
| authenticator | http://localhost:8100/admin                         |
| chooser       | http://localhost:9000                               |
| elasticsearch | localhost:9200                                      |
| keycloak      | http://localhost:8888/auth                          |
| keymaker      | http://localhost:8081/v3/api-docs                   |
| localstack    | http://localhost:4566, http://localhost:4571        |
| manifold      | http://localhost:8080                               |
| mycrossref    | http://localhost:4566/fe-bucket/index.html          |
| postgres      | localhost:5432                                      |
| prometheus    | localhost:4590                                      |
| rest-api      | http://localhost:3000 (api), localhost:7880 (nrepl) |
| console       | http://localhost:8000                               |
-----------------------------------------------------------------------------------------------

### Container naming convention

* Containers with `_integration` prefix are standalone containers that will just work, might or might not have dependencies, like for example databases (ES, mysql, postgres)

* Containers that might be standalone (last docker image in the repo) or being used for development like keycloak or cayenne, will not have prefix.

### Integration script

The `integration` script will allow you to start a "setup". A Setup is a collection of services orchestrated in a specific way.

At the end of [docker-compose-template.yml](./docker-compose-template.yml) you will see the setups. They are services suffixed with `run-` adn they are just dummy services with its own dependencies.

`docker-compose-template.yml` is not supposed to be run manually but by using the `integration` script that is used to run only the setups. When running the `integration` script it will generate a `docker-compose.yml` file from the template in order to run the services, as it need to do some replacements using `sed`.

It is more useful if you can add the path of this repository once cloned in your computer into the PATH environment variable so you can use it from anywhere. There are as well some other environment variables like `REST_API_DIR` and `KEYCLOAK_DEV_DIR` that is convenient to set as global env vars if you want to run those setups from anywhere.

* KEYCLOAK_DEV_DIR should point to the cloned idm project in your filesystem
* REST_API_DIR should point to the cloned rest_api project in your filesystem

Not all setups require env vars, `all_services` doesn't require specific ENV vars as it doesn't need to mount anything from local drives.

Usage:

* `integration all-services start` will run all the services
* `integration cayenne_dev start` will start cayenne for development
* `integration idm_dev stop` will stop idm development setup

You can list all the setups by running `integration` without parameters

## Localstack setup

There is a [directory](localstack_setup/) containing a python script that sets up localstack, that can extended to add as many resources as needed.

## Testing the REST API

We can use files available in the REST API repository: 

```bash
# From within our copy of the REST API project
awslocal s3 cp dev-resources/feeds/corpus/crossref-unixsd-ed1a42bb-3ff3-485a-af0b-511ee92dd923.body s3://md-bucket/ed1a42bb-3ff3-485a-af0b-511ee92dd923/unixsd.xml
```

A few seconds later we should be able to see data at http://localhost:3000/works

## Accessing the MyCrossref Frontend

On startup, the frontend assets are built and deployed to the `fe-bucket` S3 bucket, available locally at
http://localhost:4566/fe-bucket/index.html

### Frontend-specific environment variables

- my_crossref_auth_api_host = "https://doi.crossref.org"

